# 2. Fusion 360

## Object and Fusion 360 

On the second Module we have learned to use the FUSION 360 program from Autodesk. It is easy and free to download it online. It is the software that is used to model an object in 3D to print it later. So before talking about 3D printer (module 3) we had to draw our object.

It is not the only program to model in 3D but it is not hard to understand how to use it. It is very intuitive so once you are modeling you can discover many fonctions.

On the introduction class for this software, we have modeled together an object of FabLab: a block with round shapes on the corner and many round openings everywhere. Here follows an image showing the process of creation of this cube on the software:

![](../images/cubefusion.jpg)

**A little bit more about the class exercise:**

Here we learned to work with fillets, axes, rotations, gratings, sketches, extrusions, etc. I will quickly list them so that I can get a general idea of ​​how the program works.

Any three-dimensional modeling requires a two-dimensional sketch first, regardless of the plane (x, y or z). To do this, it must be found in the create sketch. It is with this tool that we can create shapes. Once the sketch is finished, click on finish sketch at the top right

Then we learned how to make a cube from the created shape: select the drawn sketch to extrude it. Once that is understood, it is up to us to choose an extrusion value. The possibilities are varied for extrusion. An extrusion with a negative value will remove material from the base created extrusion

To give a rounded shape to our cube, we can use the fillet function that we can find in modify. Everything must be selected and then click on leave and this is done automatically (the value is to be chosen)

To repeat the action without redoing it on each plane, you can use the network -> circular network function found in the create tab. This function is therefore used to duplicate elements according to the number and the degrees that we are going to enter. It is mandatory to select an object and an axis.

## The object from the Brussel Design Museum

We have visited the Brussels Design Museum during an afternoon and each student chose an object that have caught their attention. I have chosen a stool from Kartell that speaks a lot to me for its form, material and color. It reminds me of a childhood toy, because its playful and shiny. The object in question is called Pilastro, and, on the present day it is still produced and comercialized by Kartell.  

![](../images/kartell.jpg)

While we were there I took some measurements of the chosen object and I have also discovered it is made of thermoplastic technopolymer. It is definetetly a piece that sorts out of the room because of it's unique form.

![](../images/adam.jpg)

![](../images/croqui.jpg)

I have also figured out more about the object on Kartell's website linked here:

- [Kartell](https://www.kartell.com/US/shop-now-the-kartell-collection/pilastro/08852)


## Understanding the object in 2D

To be able to do the object in 3D, first you should understand in 2D perspective. That means, know its measurements and draw it. In my case, I have done the croquis and then I tried out a drawing on Autocad to have all the measurements cleared out to be able to start the Fusion 360. At this moment I had already an idea of how I would build my object on Fusion, so I focused on the information that would be helpful for the conception of the 3D afterwards. Fisrt, I did a sketch of a front view of the Pilastro, then I wrote down its main dimensions (lenght, width). After, I have checked how many centimeters each one of its details did. It will be easier to understand it by checking the following image:

![](../images/mesurement.JPG)

![](../images/kartellstool.JPG)

I have visualized the model as a 2D profile (skeleton) with an axe that could turn 360 degrees to build a full form. That means, I would use later on on Fusion 360, the fonction Revolve: a sketch profile around a selected axis. The profile I would have to draw on Fusion would be something like this:

![](../images/profil1.JPG)

An object that seemed complex with all its curves and organic form, at the end turned out to be simply modeled on the program. Check out the next topic to understand how I did this process.


## Understanding FUSION 360

Here follows some important commands and tips to understand the program:
The items highlighted in red are the navigation tools.

Top left: the browser, there are all the elements of the project (body, sketches, views)

Bottom left: the construction tree, it is an important element. It takes all the construction stages of the model and allows you to make a modification to the previous stages without losing the rest of the construction.

Bottom in the middle: there are all the 3D display and navigation tools.

Top right: the cube also allows you to navigate in 3D on the model by selecting the faces or even the corners of it.

## Modeling the object in 3D: FUSION 360

Once I had the good dimensions and have thought of how to execute the 3D, I was ready to start with Fusion. Therefore, I opened the software and created a new project, to be saved on the Cloud (attention: Fusion 360 only works with internet connexion, everything is saved on the Cloud). First I chose the surface I was going to work on. As I was building a profil to then rotate on the axe, I chose the Z profile (blue).

![](../images/fusiona.JPG)

I started by drawing the exact same profile as the sketch, following the good measurements. To do so, I use the Line tool. You can delete the number (measurement) that shows on the side each time you draw a line.

![](../images/fusionb.JPG)

I have done different lines for the external part of the object, to be able to divide it into parts (the rounds that pops out and the main cilinder on the center), which means 90 centimeters 5 times (5 lines).

![](../images/fusionc.JPG)

Once that was done, I did a line of guidance to then insert a circle, for the round part that pops out of Pilastro. The ray of the circunference was 4,5 cm. After that, I just had to remove the intersection lines, to be able to build an entire form at the end. To do this trim, you just need to click on the sissors of modify ab and select the lines you wish to delete.

![](../images/fusiond.JPG)
![](../images/fusione.JPG)
![](../images/fusionf.JPG)

You can finish your sketch by clicking on the buttom and the perspective of that profile will appear on the screen.

![](../images/fusiong.JPG)

Now all I had to do, was to rotate the profile around its own axis. Basically I clicked on the Revolve fonction, selected the profile and the axis. I also chose the rotation angle: 360 degrees. And it was done, Fusion showed to me the final object.

![](../images/fusionh.JPG)
![](../images/fusioni.JPG)

I have realized that working with profile and rotation/revolve was a efficient quick way to build objects. It was quite cool to see the final result of an object I admire. At the end you can also chose to change materials or colors, but I liked it how it was, so I kept it like this.

## The final object

![](../images/finalobject.JPG)

## Settings, paramètres

A last point on which modifications have been made is that of the parameterization of the object. The 'parameterize' function allows modifications to be made more efficiently to the object. The configuration menu is accessed via the 'modify' tab then 'modify the parameters'.

There, a new window opens, we find:
'user parameters': these are the parameters created for the project.
'the model parameters': there, we find all the parameters of the different functions of the drawing.

So I configure the various parameters of the object. By clicking on the + next to user settings'. We name the latter and affect characteristics. Then we apply these last parameters to the functions of the drawing, by renaming the value with the parameter created above.

For a more complete explanation, here is a tutorial: [Paramètres Fusion 360](https://www.youtube.com/watch?v=90bvJHHCd24)

## Useful links

- [Fusion 360 Autodesk](https://www.autodesk.com/products/fusion-360/personal)
- [Fusion object in STL format](objetfusioncamila.stl)
