# 3. Impression 3D

## Prusa 

To start the Module 3, we had to install software so that the machine knows how to **print our object in 3D**. For that, we went to the **Prusa** site, the designer of which gave the name to its machines. It was necessary to download _software specific to the machines in the FabLab_: the one for **PRUSA I3MK3S**. 

First **we work with files in STL format on Fusion 360**, then we import these files into the software downloaded just before, called **PrusaSlicer**. 

So once the Fusion 360 document is finished, we export it to STL and save it somewhere on our desktop. We then launch the Prusa software and import the STL document. The object will appear on it. 

From there, we can dchange some configurations of the printing so that it goes well: first we **choose the contact face between the object and the plate**, for that we can easily turn over object in all directions. The goal is obviously to minimize reinforcements and overhangs during printing. For this the contact surface must be maximum and logical in relation to the shape of the object. For my object I needed reinforcements to support, because of its curves and organic form to be built.

For the printing we use **PLA**. The bigger the object, the more time it will take to be printed. Configurations of the Prusa will also affect how long will the object take to print. Details such as the type of infill. 


## Settings of Prusa 

To properly configure 3D printing, you must already **switch to expert mode** on PrusaSlicer, this allows you to unlock additional settings and thus be **more precise** in its programming. In front of each setting appears a colored hexagon. If it is green it means the adjustment is simple, if it is orange it is an advanced adjustment, and if it is red it is an expert adjustment. You should also know that if the setting turns orange and is no longer black like the others, it has been changed. 

Take a look at the _step-by-step_ that has been learned:

1: **Layer and perimeter**: For the layer height, always put 0.2mm and do not touch that of the first layer. For the vertical walls, the perimeter must be 3 by default for the minimum in order to have a certain solidity of the object. After we are free to modify the type of interior filling, it will depend on the resistance of the expected object.

2: **Filling**: The filling density should ideally always be between 10 and 15% if the mechanical resistance of the object is low. It is important to avoid wasting material. The most effective infill shapes are gyroid and honeycomb shapes.

3 : **Border**: There is by default a programmed skirt on the object so that it is stable on the plate, but if the part that we print in 3D is large you should create an additional outline to stabilize the object during printing and prevent it from falling. That border allows to have a greater grip on the ground, it is a slight additional layer on the board, around our object, which keeps it in good shape.

4: **Supports**: we can generate additional supports for now a part of the object that would be knocked out or would need support in order not to fall.

For other settings, it is better to leave it as default and possibly play with it if you have a special case, no need to touch it. In terms of the filament, you will have to configure the machine according to the filament you insert into it.

Here are some print screens I took of my computer to **help you out** with those settings of Prusa:

![](../images/prusa1.JPG)
![](../images/prusa2.JPG)
![](../images/prusa3.JPG)
![](../images/prusa4.JPG)
![](../images/prusa5.JPG)
![](../images/prusa6.JPG)

## Time to print!

Once everything looks good, we **export the G-code from PursaSlicer**, this G-code should be copied to an **SD card of one of the printers, in a folder with our name.surname**. The folder being on the SD card, the machine will detect it and offer us to open the file. 

Make sure to **clean the plate** with acetone before use is essential to ensure maximum adhesion to the part on them. Stay close at least to see if the first layers are going well. Otherwise, you will have to stop printing. 

You also have to be **very careful** with the printing speed and ideally **never exceed 100%**, the slower the machine the better, especially at the beginning. 

![](../images/3dprinter1.jpg)
![](../images/3dprinter2.jpg)
![](../images/3dprinter3.jpg)

## Result

**Bingo!** Luckily, everything went well on the first try! That means that the settings on Prusa were corresponding to the type of object I had to print. I have done the configurations a way the object would print well and also not take so long, that means, find a balance!

The little "Pilastro" by Kartell was the object I have printed, taking about **2 hours**. It has about **2x4 cm** with green **PLA fillament**. Once finally the printing ended successfully, I had to gently take off the reinforcements because the axis of rotation. I had to be very careful with this.

I really enjoyed the Module 3 and to see the result of all the objects printed by us on Fablab.

![](../images/finalobject3d.jpg)

## Link to download Prusa

- [Prusa 3d](https://www.prusa3d.com/)


## Other useful links

- [Tuto: How to export an STL file from Fusion 360](https://knowledge.autodesk.com/fr/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/FRA/How-to-export-an-STL-file-from-Fusion-360.html)
- [STL file](../images/objetfusioncamila.stl)
