# Final Project: Kids @ Brussels Design Museum

During the last weeks we have been working on our final project idea. As a part of the Module 3, I participate on the conception of a project that tries to introduce design to kids and to make them feel more interested by the subject. We work with a partnership with the Brussels Design Museum, where this project takes place. 

To start, we have divided the groups. Each group works in a project that will catch the kids attention to the objects exposed in the museum. Our group, composed by Louise, Louison and I, have decided to work with miniatures, as we believe those can speak a lot to kids. They are finally able to touch those objects they have seen on their visit. Besides, it is possible to see details closer, to understand its forms and also, it's an ideal size for a game for example.

Therefore, we decided to search, model and create those miniatures with the 3D printers available on Fablab. This was the point of depart, that was after going to developp into a instructionnal board game.

## Week 1: First ideas

On the first week of work we had many questions regarding our idea... We knew we wanted for sure to work with the miniatures, but what else could we develop in a way that the kids could actually learn something useful. That means, an educational game. We had some ideas in mind, such as work with colors, textures, touch, maybe a timeline... still very general, so we had to do see what would work the best for an educational purpose.

The first idea was to use cards that graphically represented the different characteristics of the objects of the museum that we would have printed out in 3D. The cards would be then, in correspondence with one of the miniatures.

![](docs/images/cartes1.jpg)

During this phase, we still didn't have anything concrete, we were mostly trying out things, but the idea or caracteristics seemed very interesting to us, as the kids would work in their memory, focus and understanding of the visit. We still had to decide how many and which objects we would use for the game in miniature form. It would be impossible to go over them all in a game, so we already had in mind a number between 8 and 12. The children would play the game after the visit. 

## Week 2: Research and choice of objects

During the second week, we created a list of the chosen objects: we tried to select an "iconic" object of each room / podium and therefore of each plastic. We arrived on a number of 8 objects that we have choosen and that we believed to be the most attractive ones for the kids to get to know better. Then we have started to model those objects on Fusion and print them out on the Fablab 3d machines. Some files took to 12 hours to print. We had a few printings that failed but finally when restarted, it worked. 

From those objects we could print out 6 so far. We decided to keep this number as a test for the first try-out of the game with the kids, and then we will see if we add the 2 last objects we have thought of, or if it is already enough for the game time, etc. The objects we chose and printed out are the following ones, next to its location in the museum and its features:

- Cantilever by Verner Panton: Podium "overhang". Features: Technical prowess - resistant - overhang

![](docs/images/cantilever.jpg)

- Dondolo by Cesare Leonardi et Franca Stagi: "Space age" room – GRP (fiberglass). Features: Futuristic/space - Rocking chair - Anti-gravity

![](docs/images/dondolo.jpg)

- Pratone by Pietro Derossi, Giorgio Ceretti et Riccardo Rosso: Podium "radical design" - PUR. Features: Soft - Bigness - Nature look alike

![](docs/images/pratone.JPG)

- Endless flow by Dirk Vander Kooij: Podium "recycled". Features: Recycled - Fridge - 3D printing

![](docs/images/endlessflow.JPG)

- Translation by Alain Gilles: Podium "recycled". Features: Recycled - Composed of lots of small objects - Multicolored 

![](docs/images/translation.jpg)

- Chica by Jonathan de Pas, Donato d'Urbino, Paolo Lomazzi, Giorgio de Curso: First room- ABS. Features: Moldable - Stackable - For kids

![](docs/images/chica.JPG)

Finally a referencing of their characteristics has also been done. We picked up colorful illustrations from an specific site, because we thought those images were more related to a kid then the first pictograms we used. At the end, together with the Fablab team we realized it would be better to keep with the pictograms, to keep it minimalist and simple, but to add maybe a key-word next to the image for an easier understanding for the kids (to see on Week 3). Here are the cards we started to draw:

![](docs/images/cards1.JPG)
 
And here are the rules we have setted up for it:

![](docs/images/regles1.JPG)
 
At this point, we have realized there was something very important missing to our game: a scenario and a base. We needed a support, something that would make it really look like a game and not just like a school activity. We should also improve the game, by adding more to it, maybe creating more rules, more funny things, complicating it a little bit as well... Those were some of the things we have developed on the week 3.

## Week 3: Improving and adding more to the game

This week we have worked hard to get to something that we believe it is much more interesting. We have thought of something anyone could play even if we were not present; a game that had more a face of a game and less of a school activity idea. We thought of a scenario and we made sure to have a base.

We thought it would be nice to use the plan of the Museum as a board, since we were also using the objects in miniature, it made sense to put those together and create a sort of a miniature museum game, where you walk through the museum passing by the miniatures. The game works as a regular boarding game, where there are pawns for each player, dices and the board with a path to follow as you play the game. It also counts with cards, that are divided into 3 cathegories: history, caracteristics and bonus/curiosities. Each one corresponds of a color, and once you stop in the square with one of those colors, you answer a question from the corresponding cathegory. 

Here are the colored cards we have designed and printed out:

![](docs/images/cartes2.jpg)
![](docs/images/cartes3.jpg)

We thought this way the game could be dinamic and that by adding some funny details to it, such as the designers of the miniatures as the players (pawns), the kids would not only have fun, but also learn and discover more of the BDM. 

![](docs/images/designers.jpg)

On the cards we also thought to add 3 options of answer, so the game could flow more easily and the kid would have some indications. Finally, for the price we modeled some 'granulés' and printed them out on 3d, to work as the points to take once you have the good answer, and at the end, once all the 'granulés' are taken, the class can get a common prize! It reinforces the team spirit and it could be a healthier 'competition' for the kids, knowing everyone is contributing.

The 'granulés'/price being printed:

![](docs/images/granules.jpg)

And once they were ready, in the box we made, where we have put the group prize:

![](docs/images/boitegranules1.jpg)

The players, with the head of the designers (the base was cutted out with the Epilog):

![](docs/images/designer.jpg)
 
For the board and the cards, we have designed them ourselves, on computer programs; and then we printed them out with a thicker paper. But for the final game, we would like to improve the quality of printing and have something harder and maybe plastified, to protect from dirt. We also count on making different boxes to put the components of the game and have everything organised in a big box. 

Here follows the scenario and the rules of the game we are going to try this week with the kids:

![](docs/images/regles2.jpg)
 
Here a few pictures of the board of the game, where we put the miniatures how they are actually placed in the museum:

![](docs/images/jeu1.jpg)
![](docs/images/jeu2.jpg)
![](docs/images/jeu3.jpg)
![](docs/images/jeu4.jpg)
 
We are looking forward now to see how it will go and to see the reaction of the kids!

## Testing the game with the Kids: 02/12 on the Fablab 

The game experience with the kids went better than expected. They were very receptive and excited for the game. Once they saw the board they already recognized the plan of the museum and the chairs that they have seen during their visit. We took a couple minutes to explain the rules and to start the game. They chose their 'designer' player and during the path they answered the questions corresponding to the color of each case they stopped by. They were respectful to their friends and they also helped each other out when their collegue didn't know an answer. We could see they really payed attention to the visit they did some weeks ago and that they have a good memory for colors and other visual things: textures, format, etc. We took around 20-25 minutes with each group to finish the game (arrive at the end). Some of the cards were harder than others, so we count to adapt this and put them all on the right level. In general, it works pretty well, they have fun and they learn. So we will mostly focus on the design of the game for the next weeks, such as improving the board, etc. We have played the game with 6 groups of 2 or 3 students each, during the morning and afternoon. It was a fun day for the kids but for us as well!

Some registers of today with the kids:

![](docs/images/jeuenfants1.jpg)
![](docs/images/jeuenfants2.jpg)
![](docs/images/jeuenfants3.jpg)

Here is a little debriefing of what works and what we should improve in the game:

WORKS WELL:

- the game itself worked well: the idea of "being in the museum" in the game (working with space: being located), the board with the boxes/pawns/cards, the number of boxes of the game and cards was ideal;
- they were curious and they remembered the visit and they learned new things;
- they wanted to get to the end, continue the game
- curious by the final price; the badgets worked well
- helping each other worked well, team spirit

TO BE IMPROVED FOR THE NEXT TIME:​

- print objects in the real color as it is in the museum;
- show the object name on the board, next to the 3d;
- more of the green cards (caracteristics);
- give leads in the question (ex: ''when did the BDM open its doors... + knowing that it is a fairly recent museum");
- more distant responses;
- change on the rules: green card 1 point, blue 2, red 3;
- price: 6 small "mini miniatures";
- storage boxes and the design of the board (3 a1 of wood for the board?, improve the cards and the 3d pawns should be more resistant);


## Week 4: doing the changes!

This week we have taken the time to seat and discuss what should we do for our pre jury. We analyzed the list of what we should improve. As time was short and machines were limitred to do everything we actually want to do for the final jury, we have been reasonable on what was more important and urgent to change. So we focused on the game board and on its box.

For the game board we struggled to how we were going to make it foldable without damanging the boad at some point. We also couldn't find the exact measures of 'carton' that we needed, as our game has the specific measurements of the museum. So we came up with a solution that would bring the kids to one more exercise for the game. We decided to cut out the pieces of the board into puzzle parts, which were going to stick to each other to form the museum's plan. So they would also work on their visual and spacial memory. We created big puzzle pieces cutted out with Laserseur. First, we draw the plan with its puzzle divisions, so we could make sure which sizes of 'carton' we needed. Then we passed our file to SVG to be able to cut with the laser machine. We did all the scale adjustments before and luckly everything worked well! We used the 800 speed for the and 40 for the intensity on the machine's settings.

We printed the plan in a better paper this time, and once the pieces of the puzzle / plan were cutted out, we cutted out the plan to glue it into each corresponding piece. Now we had a real board game! 

Check here:

![](docs/images/a8.jpg)
![](docs/images/a10.jpg)
![](docs/images/a6.jpg)

For the cards, we did the changings on some of the questions, giving some tips to the kids to find the good answer. We also added up some of the green cards, as we observed that they were not enough last time. Those were also printed out again in a better and more rigid paper. Green cards added to the game:

![](docs/images/a4.jpg)

For the miniatures, we had to print some of them out again, to have them as the original colors of the objects in the museum, making the game as close as possible to the reality. Unfourtunetly, there is still one chair to be printed out in its real color, the Cantilever. The printing failed so we will have to restart... For the miniatures: we also started to printed out mini versions of our miniatures. Those are supposed to be the prize we will give to each group of kids that play the game! 

![](docs/images/a5.jpg)

For the 'pions' we decided to also print them out in 3D, to compose the game and for a better rigidity. We have modeled them on Fusion 360 and then we did a first try out that worked! Them we put the rest to print on the 3d machine. We haven't been to Fablab since we asked the machine to print (when we left it was in the middle of the process), so we cross our fingers for this thursday to arrive and see them there! 

![](docs/images/a1.jpg)
![](docs/images/a7.jpg)
![](docs/images/a9.jpg)

And last, for this time, we have worked to create a beautiful box to put the game and everything inside! The box is made out of wood, cutted out in the machine. Some of the registers of the process at Louison's house:

![](docs/images/a11.jpg)
![](docs/images/a12.jpg)
![](docs/images/a13.jpg)

For the final jury we still have to improve some things, such as decorate the box, create small boxes or compartiments to keep the small pieces of the game, etc. But we are happy with the progress of the last two weeks and we have the final game idea ready to be built! 

